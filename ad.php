<?php 
header("Content-Type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=Active Directory" . date('d-m-Y') . ".xls");
$ldap_dn ="cn=read-only-admin,dc=example,dc=com";
$ldap_password = "password";

$ldap_con = ldap_connect("ldap.forumsys.com");
ldap_set_option($ldap_con, LDAP_OPT_PROTOCOL_VERSION, 3);
if(@ldap_bind($ldap_con,$ldap_dn,$ldap_password))
{

	 $filter = "cn=*";
	 //$justthese = array("displayName","telephoneNumber","homePostalAddress","departmentNumber","mail","employeeType","givenName","mobile");
	 $result = ldap_search($ldap_con,"dc=example,dc=com",$filter) or exit("Unable to search");
	 $entries = ldap_get_entries($ldap_con,$result); 
	 ?>
	 <table border="1">
	   <tr>
	       <td> First Name </td>
		   <td> Last Name </td>
		   <td> Email </td>
		   <td> Number </td>
		   <td> Department </td>
		   <td> Location </td>
		   <td> Designation </td>
	   </tr>
	<?php for($i=0;$i<count($entries);$i++){ ?>
	   	<tr>
	       <td><?php  if(!empty($entries[$i]['cn'][0])){ echo $entries[$i]['cn'][0]; } ?></td>
		   <td> <?php if(!empty($entries[$i]['sn'][0])){ echo $entries[$i]['sn'][0]; } ?> </td>
           <td> <?php if(!empty($entries[$i]['mail'][0])){ echo $entries[$i]['mail'][0]; } ?> </td>
           <td> <?php 	 if(!empty($entries[$i]['telephonenumber'][0])){ echo $entries[$i]['telephonenumber'][0]; } ?> </td>
           <td> <?php if(!empty($entries[$i]['ou'][0])){ echo $entries[$i]['ou'][0]; } ?> </td>
           <td> <?php if(!empty($entries[$i]['postalAddress'][0])){ echo $entries[$i]['postalAddress'][0]; } ?> </td>
		   <td> <?php if(!empty($entries[$i]['designation'][0])){ echo $entries[$i]['designation'][0]; } ?> </td>
	   </tr>
	 <?php } ?>
	 </table>

   <?php  }else{
	       echo "Not Connect To Server";
    }
       
?>

